package com.latif.spring.servlet3.entity;

/**
 * Created by LATIF on 4/21/2015.
 */
public class Mahasiswa {

    private Long id;
    private String nim;
    private String nama;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
