package com.latif.spring.servlet3.service.impl;

import com.latif.spring.servlet3.entity.Mahasiswa;
import com.latif.spring.servlet3.service.MahasiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LATIF on 4/21/2015.
 */
@Service
public class MahasiswaServiceMysql implements MahasiswaService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Mahasiswa create(Mahasiswa mahasiswa) {
        jdbcTemplate.update("insert into mahasiswa(`nim`,`nama`) VALUES (?,?)", mahasiswa.getNim(), mahasiswa.getNama());
        return mahasiswa;
    }

    @Override
    public Mahasiswa update(Mahasiswa mahasiswa) {
        jdbcTemplate.update("update mahasiswa set `nim`=?, `nama`=? WHERE `mahasiswa_id` = ?", mahasiswa.getNim(), mahasiswa.getNama(), mahasiswa.getId());
        return mahasiswa;
    }

    @Override
    public List<Mahasiswa> findAll() {
        SqlRowSet rowSet = jdbcTemplate.queryForRowSet("SELECT `mahasiswa_id`, `nim`, `nama` FROM mahasiswa");
        List<Mahasiswa> result = new ArrayList<>();
        while(rowSet.next()){
            Mahasiswa mahasiswa = new Mahasiswa();
            mahasiswa.setId(rowSet.getLong("mahasiswa_id"));
            mahasiswa.setNim(rowSet.getString("nim"));
            mahasiswa.setNama(rowSet.getString("nama"));
            result.add(mahasiswa);
        }
        return result;
    }

    @Override
    public Mahasiswa findOne(Long id) {
        SqlRowSet rowSet = jdbcTemplate.queryForRowSet("SELECT `mahasiswa_id`, `nim`, `nama` FROM mahasiswa where `mahasiswa_id` = ?", id);
        if (rowSet.next()){
            Mahasiswa mahasiswa = new Mahasiswa();
            mahasiswa.setId(rowSet.getLong("mahasiswa_id"));
            mahasiswa.setNim(rowSet.getString("nim"));
            mahasiswa.setNama(rowSet.getString("nama"));
            return mahasiswa;
        }
        return null;
    }

    @Override
    public void delete(Long id) {
        jdbcTemplate.update("delete from mahasiswa where `mahasiswa_id`=?", id);
    }

    @Override
    public void delete(Mahasiswa mahasiswa) {
        delete(mahasiswa.getId());
    }
}
