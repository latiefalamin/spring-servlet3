package com.latif.spring.servlet3.service;

import com.latif.spring.servlet3.entity.Mahasiswa;

import java.util.List;

/**
 * Created by LATIF on 4/21/2015.
 */
public interface MahasiswaService {
    public Mahasiswa create(Mahasiswa mahasiswa);
    public Mahasiswa update(Mahasiswa mahasiswa);
    public List<Mahasiswa> findAll();
    public Mahasiswa findOne(Long id);
    public void delete(Long id);
    public void delete(Mahasiswa mahasiswa);
}
